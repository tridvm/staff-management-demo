const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const  UserController = require('../App/Controllers/Http/UserController');


router.get('/:id', (req, res, next) => {
    UserController.getUserInfoById(req, res, next);
});

router.put('/:id', (req, res, next) => {
    UserController.updateUserById(req, res, next);
});

module.exports = router;