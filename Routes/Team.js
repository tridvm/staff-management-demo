const express = require('express');
const AuthenMiddleware = require('../App/Middlewares/AuthMiddleware');

const router = express.Router();

router.use(AuthenMiddleware.auth);

router.get('/', (req, res, next) => {
    console.log('Hello', req.user);

    return res.status(200).json({
        message: 'Get team successfully!',
        data: null
    })
})

module.exports = router;