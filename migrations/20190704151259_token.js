
exports.up = function(knex) {
    return knex.schema.createTable('tokens', function (table) {
        table.increments('id');
        table.integer('user_id').notNullable();
        table.string('token').notNullable();
        table.string('status').notNullable();
        table.timestamps(true, true);
        
     })
};

exports.down = function(knex) {
  
};
