
exports.up = function(knex) {
  return knex.schema.createTable('users', function (table) {
    table.increments('id');
    table.string('name', 80).notNullable();
    table.string('username', 30).unique().notNullable();
    table.string('password', 30).notNullable();
    table.timestamps(true, true);
 })
};

exports.down = function(knex) {
  
};
