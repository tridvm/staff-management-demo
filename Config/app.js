const express = require('express');
const app = express();
const http = require('http').Server(app);
const bodyParser = require('body-parser');

require('dotenv').config();
global.ENV = process.env;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



http.listen(ENV.PORT, (err) => {
    if (err) {
        throw new Error('Errors occur!');
    }

    console.log(`Server is running on port ${ENV.PORT}`);
});

const io = require('socket.io')(http, {
    pingTimeout: 30000,
    pingInterval: 60000,
});

let connected = [];

io.on('connect', (socket) => {
    const userInfo = {
        id: socket.id,
        name: socket.request._query.name || null,
     }
    connected.push(userInfo);
    io.emit('client-connect', userInfo);
    console.log('ssss',connected);
    console.log('connetect: ', socket.request._query.name)

    socket.on('chat-send', (data) => {
        const info = connected.filter(el => el.id === socket.id)[0];
        console.log(info);
        io.emit('chat-receive', { data, id: socket.id, name: info.name, timestamp: Date.now() });
    })

    socket.on('is-typing', () => {
        const info = connected.filter(el => el.id === socket.id)[0];
        console.log('isTypeing', info);
        io.emit('show-typing', { id: socket.id, name: info.name });
    })

    socket.on('stop-typing', () => {
        const info = connected.filter(el => el.id === socket.id)[0];
        console.log('isTypeing', info);
        io.emit('hide-typing', { id: socket.id, name: info.name });
    })

    socket.on('disconnect', (data) => {
        console.log('BEFORE DISConneted', connected);
        let exitInfo = {};

        connected = connected.filter(el => {
            if (el.id !== socket.id) {
                return el;
            } else {
                exitInfo = el;
            }
        });
        console.log('DISConneted', connected);
        io.emit('client-disconnect', exitInfo);
    })
})

module.exports = app;