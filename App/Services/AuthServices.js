const UserModel = require('../Models/UserModel');
const TokenModel = require('../Models/TokenModel');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

class AuthServices {
    constructor() {
        this.userModel = UserModel;
        this.tokenModel = TokenModel;
    }

    async register(body) {
        try {
            const { username, password } = body;
            if (!username || !password) {
                return {
                    message: 'Username or password is required!',
                    data: null
                }
            }
    
            const user = await this.userModel.query()
            .where('username', username)
            .first();
    
            if (user) {
                return {
                    message: 'Username existed!',
                    data: null
                }
            }

            const hash = await bcrypt.hash(password, 10);
    
            const dataInsert = {
                username,
                password: hash,
                name: username
            }
    
            const userInsert = await this.userModel.query().insert(dataInsert);
    
            const token = jwt.sign({
                id: userInsert.id,
                timestamp: Date.now,
            } ,ENV.APP_KEY)
    
            const dataTokenInsert = {
                user_id: userInsert.id,
                token,
                status: 1
            };
    
            await this.tokenModel.query().insert(dataTokenInsert);

            return {
                message: 'Register successfuly!',
                data: token,
            }
        } catch(err) {
            console.log(err);
            return {
                message: 'Errors occur!',
                data: null,
            }
        }
    }


    async login(req, res, next) {
        try {
            const { username, password } = req.body;
            console.log(userModel)
            if (!username || !password) {
                return res.json({
                    message: 'Username or password is required!',
                    data: null
                })
            }

            const user = await this.userModel.query()
                .where('username', username)
                .first();

            if (!user) {
                return res.json({
                    message: "Username or password is incorrect!",
                    data: null
                })
            }

            const paswordChecked = bcrypt.compare(password, user.password);

            if (paswordChecked) {
                const token = jwt.sign({
                    id: user.id,
                    timestamp: Date.now,
                }, ENV.APP_KEY)

                await this.tokenModel.query().insert({
                    user_id: user.id,
                    token,
                    status: 1
                });

                return res.json({
                    message: "Login successfully!",
                    data: token
                })
            }
        } catch(err) {
            console.log('Lỗi: ', err);
        }
    }

    async logout(req, res, next) {
        const { authorization } = req.headers;
        
        if (!authorization) {
            return res.status(400).json({
                message: 'Logout Failed!',
                data: null
            })
        }

        const isDeleted = await this.tokenModel.query().delete().where('token', authorization.replace("Bearer ", ''));
        


        if(!isDeleted) {
            return res.status(400).json({
                message: 'Logout Failed!',
                data: null
            })
        }

        return res.json({
            message: 'Logout successfully!',
            data: null
        })
    }
}

module.exports = new AuthServices();