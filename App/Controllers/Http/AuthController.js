const AuthServices = require('../../Services/AuthServices');

class AuthController {
    constructor() {
        this.authServices = AuthServices;
    }

    async register(req, res, next) {
        const { body } = req;

        const result = await this.authServices.register(body);
        
        console.log(result);

        return res.json(result);
    }


    async login(req, res, next) {
        const { body } = req;

        const result = await this.authServices.login(body);
        
        console.log(result);

        return res.json(result);
    }

    async logout(req, res, next) {
        const { headers } = req;

        const result = await this.authServices.logout(headers);
        
        console.log(result);

        return res.json(result);
    }
}

module.exports = new AuthController();