const UserModel = require('../../Models/UserModel');

class AuthController {
    constructor() {
        this.userModel = UserModel;
    }

    async getUserInfoById(req, res, next) {
        const { id } = req.params;

        if (!id) {
            return next({
                message: 'Empty id!',
                data: null
            })
        }

        const user = await UserModel.query().findById(id);
        console.log(user)
        if (!user) {
            return res.status(404).json({
                message: 'User not found!',
                data: null
            })
        }

        return res.json({
            code: 0,
            data: user
        })
    }

    async updateUserById(req, res, next) {
        const { id } = req.params;
        const { name } = req.body;

        if (!id) {
            return next({
                message: 'Empty id!',
                data: null
            })
        }

        if (!name) {
            return next({
                message: 'Empty name!',
                data: null
            })
        }
        

        const user = await UserModel.query().patch({ name }).where('id', id);
        if (!user) {
            return res.status(404).json({
                message: 'User not found!',
                data: null
            })
        }

        return res.json({
            message: `Update user id ${id} successfully!`,
            data: null
        })
    }

    
}

module.exports = new AuthController();