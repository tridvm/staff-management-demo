const { body } = require('express-validator');

module.exports = [
    body('username')
    .exists()
    .withMessage('User is required!')
    .not()
    .isEmpty()
    .withMessage('Username is required!'),
    body('password')
    .exists()
    .withMessage('Password is required!')
    .isLength({
        min: 6,
        max: 30,
    })
    .withMessage('Password must be at 6 - 30 characters!'),
]