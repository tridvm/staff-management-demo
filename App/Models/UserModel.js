const Model = require('./Model');

class UserModel extends Model {
    constructor() {
        super();
    }

    static getTableName() {
        return 'users';
    }
}

module.exports = UserModel;