const Model = require('./Model');

class TokenModel extends Model {
    constructor() {
        super();
    }

    static getTableName() {
        return 'tokens';
    }
}

module.exports = TokenModel;