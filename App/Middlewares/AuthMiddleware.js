const jwt = require('jsonwebtoken');

class AuthMiddleware {
    constructor() {

    }

    // needing authen to access
    /**
     * authen function
     * @description: Middleware check authentication of user
     * @param req
     * @param res
     * @param next
     */
    auth(req, res, next) {
        const { authorization } = req.headers;

        try {
            if (!authorization) {
                return next({
                    message: 'Forbidden!',
                    data: null 
                });
            } else {
                const decoded = jwt.verify(authorization.replace("Bearer ", ''), ENV.APP_KEY);

                if (!decoded) {
                    return next({
                        message: 'Invalid token!',
                        data: null
                    })
                }
                console.log(decoded);
        
                req.user = decoded;
                next();
            }
            
        } catch(err) {
            console.log(err)
            return res.status(500).json({
                message: 'Failed to verify token!',
                data: null
            })
        }
    }

    // no need authen to access
    noAuth() {
        
    }
}

module.exports = new AuthMiddleware();